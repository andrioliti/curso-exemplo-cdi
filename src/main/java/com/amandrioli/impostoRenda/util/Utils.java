package main.java.com.amandrioli.impostoRenda.util;

import java.util.ResourceBundle;

public class Utils {

	private Utils() { }
	
	public static <T> T getInstance(String className, Class<T> clasType) {
		try {
			Class<?> classe = Class.forName(className);
			
			return classe
				.asSubclass(clasType)
				.newInstance();

		} catch (Exception e) {
			return null;
		}
	}
	
	
	public static String getConfig(String param) {
		ResourceBundle resource = ResourceBundle.getBundle("main.java.com.amandrioli.impostoRenda.config.dependencias");
		
		if (resource.containsKey(param)) {
			return resource.getString(param);
		}
		
		return "";
	}
}
	
