package main.java.com.amandrioli.impostoRenda.model;

public class Funcionario {

	private String nome;
	private Double salario;
	private EscolaridadeEnum escolaridade;
	private Integer tempoEmpresa;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getSalario() {
		return salario;
	}

	public void setSalario(Double salario) {
		this.salario = salario;
	}

	public EscolaridadeEnum getEscolaridade() {
		return escolaridade;
	}

	public void setEscolaridade(EscolaridadeEnum escolaridade) {
		this.escolaridade = escolaridade;
	}

	public Integer getTempoEmpresa() {
		return tempoEmpresa;
	}

	public void setTempoEmpresa(Integer tempoEmpresa) {
		this.tempoEmpresa = tempoEmpresa;
	}

}
