package main.java.com.amandrioli.impostoRenda;

import main.java.com.amandrioli.impostoRenda.model.EscolaridadeEnum;
import main.java.com.amandrioli.impostoRenda.model.Funcionario;
import main.java.com.amandrioli.impostoRenda.service.CalculadorImposto;

public class ImpostoRendaMain {
	
	public static void main(String[] args) {
		Funcionario func = new Funcionario();
		
		func.setNome("João");
		func.setSalario(1300.0);
		func.setEscolaridade(EscolaridadeEnum.SUPERIOR);
		func.setTempoEmpresa(2);
		
		CalculadorImposto calcImposto = new CalculadorImposto();
		
		// injeção de dependência
		System.out.println(calcImposto.calculaImposto(func));
	}
	
}

