package main.java.com.amandrioli.impostoRenda.service;

import main.java.com.amandrioli.impostoRenda.model.Funcionario;

public class CalculadorImposto {

	public Double calculaImposto(Funcionario func) {
		// se salario < 1500 nada a declara
		// se salario >= 1500 < 3000 05% sem deducao
		// se salario > 3000 15% deducao 100
		// IoC
		
		CalculadoraSalarioInterface calculadoraSalario =
				CalculadoraSalarioFactory.getCalculadoraSalario();

		Double imposto = 0.0;
		Double deducao = 0.0;
		Double salario = calculadoraSalario.calculaSalario(func);
		
		if (salario >= 1500.0 && salario < 3000.0) {
			imposto = salario * 0.05;
		}
		
		if (salario >= 3000) {
			imposto = salario * 0.15;
			deducao = 100.0;
		}
		
		return (imposto - deducao);
	}
	
}
