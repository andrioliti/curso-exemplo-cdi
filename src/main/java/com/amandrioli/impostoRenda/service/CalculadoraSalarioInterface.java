package main.java.com.amandrioli.impostoRenda.service;

import main.java.com.amandrioli.impostoRenda.model.Funcionario;

public interface CalculadoraSalarioInterface {

	Double calculaSalario(Funcionario func);
	
}
