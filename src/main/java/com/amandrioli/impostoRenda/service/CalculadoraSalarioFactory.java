package main.java.com.amandrioli.impostoRenda.service;

import main.java.com.amandrioli.impostoRenda.util.Utils;

//container de injeção de dependência de calculadora de salários
public class CalculadoraSalarioFactory {

	private static final String KEY = "calculadoraClass";
	
	private CalculadoraSalarioFactory() { }
	
	//instanciar genericamente nossa calculadora
	public static CalculadoraSalarioInterface getCalculadoraSalario() {
		return Utils.getInstance(Utils.getConfig(KEY),
				CalculadoraSalarioInterface.class);
	}
	
}
