package main.java.com.amandrioli.impostoRenda.service;

import main.java.com.amandrioli.impostoRenda.model.EscolaridadeEnum;
import main.java.com.amandrioli.impostoRenda.model.Funcionario;

public class CalculadoraSalario implements CalculadoraSalarioInterface {

	@Override
	public Double calculaSalario(Funcionario func) {
		// 1 % de aumento no salario para cada ano de empresa
		// 10% pra funcionarios com mais de 5 anos
		// 20% a mais para funcionarios com superior completo
		
		
		Double salario = func.getSalario();
		
		if (func.getEscolaridade() == EscolaridadeEnum.SUPERIOR) {
			salario = salario * 1.2;
		}
		
		if (func.getTempoEmpresa() >= 5) {
			salario = salario * 1.1;
		}
		
		salario = salario * (1 + func.getTempoEmpresa()/100.0);
		
		return salario;
	}
	
}
